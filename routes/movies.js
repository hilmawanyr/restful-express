// Joi for input validation
const Joi = require('joi');
// require express
const express = require('express');
// instance express
const router = express.Router();
// require body parser to handle HTTP post
const bParser = require('body-parser');

// use middleware bodyparser to handle HTTP post
router.use(bParser.urlencoded({extended: false}));

// make empty array for store data
const movies = [];

// show list all movies
router.get('/', (req, res) => {
    // return movie list
    res.send(movies);
});

// get specify movie
router.get('/:id', (req, res) => {
    // is that movie exist?
    let isExist = movies.find(m => m.id === parseInt(req.params.id));
    if (!isExist) return res.status(404).send('No movies found!');
    res.send(isExist);
});

// store movie to empty array
router.post('/', (req, res) => {
    // check validity of request body
    const isValid = validatePost(req.body);
    if(isValid.error) return res.send(isValid.error);

    // store movie into new variable
    const newMovie = {
        id: movies.length + 1,
        name: req.body.moviename
    }

    // store movie to empty array
    movies.push(newMovie);
    res.send(newMovie);
});

// grab specify movie and edit it
router.put('/:id', (req, res) => {
    // is movie exist?
    let catchMovie = movies.find(m => m.id === parseInt(req.params.id));
    if (!catchMovie) return res.send('Movie not found');

    // is body request valid?
    let validate = validatePost(req.body);
    if (validate.error) return res.send(validate.error);

    // edit data
    catchMovie.name = req.body.moviename;
    res.send(catchMovie);
});

// remove movie by its id
router.delete('/:id', (req, res) => {
    let isExist = movies.find(m => m.id === parseInt(req.params.id));
    if (!isExist) return res.send('No movie was found!');

    // get index movie bi its id
    let index = movies.indexOf(isExist);
    // remove movie from array
    movies.splice(index, 1);
    res.send(movies);
});

// make validation input by Joi
function validatePost(movie) {
    // make validation schema
    const schema = {
        moviename: Joi.string().min(3).required()
    }
    return Joi.validate(movie, schema);
}

module.exports = router;