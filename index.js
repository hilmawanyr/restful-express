// load debug for debugging
const debuger = require('debug')('app:movieApp');
// movie route
const movies = require('./routes/movies');
// configuration
const config = require('config');
// load morgan
const morgan = require('morgan');
// require express
const express = require('express');
// instance express
const app = express();

// use morgan to log HTTP request
app.use(morgan('tiny'));

// config
debuger('App name: ' + config.get('name'));

// use movies router
app.use('/api/movies', movies);

// root URL
app.get('/', (req, res) => {
    res.send("Welcome to API for list movies!");
});

// listen port
const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`App running on port ${port}`);
});